" Supporting code -------------------------------------------------------------
" Initialisation: {{{

if version > 580
  hi clear
  if exists("syntax_on")
    syntax reset
  endif
endif

let g:colors_name='gruvcrate'

if !(has('termguicolors') && &termguicolors) && !has('gui_running') && &t_Co != 256
  finish
endif

" }}}
" Global Settings: {{{

if !exists('g:gruvcrate_bold')
  let g:gruvcrate_bold=1
endif
if !exists('g:gruvcrate_italic')
  if has('gui_running') || $TERM_ITALICS == 'true'
    let g:gruvcrate_italic=1
  else
    let g:gruvcrate_italic=0
  endif
endif
if !exists('g:gruvcrate_undercurl')
  let g:gruvcrate_undercurl=1
endif
if !exists('g:gruvcrate_underline')
  let g:gruvcrate_underline=1
endif
if !exists('g:gruvcrate_inverse')
  let g:gruvcrate_inverse=1
endif

if !exists('g:gruvcrate_guisp_fallback') || index(['fg', 'bg'], g:gruvcrate_guisp_fallback) == -1
  let g:gruvcrate_guisp_fallback='NONE'
endif

if !exists('g:gruvcrate_improved_strings')
  let g:gruvcrate_improved_strings=0
endif

if !exists('g:gruvcrate_improved_warnings')
  let g:gruvcrate_improved_warnings=0
endif

if !exists('g:gruvcrate_termcolors')
  let g:gruvcrate_termcolors=256
endif

if !exists('g:gruvcrate_invert_indent_guides')
  let g:gruvcrate_invert_indent_guides=0
endif

if exists('g:gruvcrate_contrast')
  echo 'g:gruvcrate_contrast is deprecated; use g:gruvcrate_contrast_light and g:gruvcrate_contrast_dark instead'
endif

if !exists('g:gruvcrate_contrast_dark')
  let g:gruvcrate_contrast_dark='medium'
endif

if !exists('g:gruvcrate_contrast_light')
  let g:gruvcrate_contrast_light='medium'
endif

let s:is_dark=(&background == 'dark')

" }}}
" Palette: {{{

" setup palette dictionary
let s:gb = {}

" fill it with absolute colors
let s:gb.dark0_hard  = ['#1d2021', 234]     " 29-32-33
let s:gb.dark0       = ['#272b34', 235]     " 40-40-40
let s:gb.dark0_soft  = ['#32302f', 236]     " 50-48-47
let s:gb.dark1       = ['#36383c', 237]     " 60-56-54
let s:gb.dark2       = ['#504D45', 239]     " 80-73-69
let s:gb.dark3       = ['#545A66', 241]     " 102-92-84
let s:gb.dark4       = ['#7C7464', 243]     " 124-111-100
let s:gb.dark4_256   = ['#7c6f64', 243]     " 124-111-100

let s:gb.gray_245    = ['#928874', 245]     " 146-131-116
let s:gb.gray_244    = ['#928874', 244]     " 146-131-116

let s:gb.light0_hard = ['#f9f5d7', 230]     " 249-245-215
let s:gb.light0      = ['#fbebc7', 229]     " 253-244-193
let s:gb.light0_soft = ['#f2e5bc', 228]     " 242-229-188
let s:gb.light1      = ['#ebd9b2', 223]     " 235-219-178
let s:gb.light2      = ['#d5c5a1', 250]     " 213-196-161
let s:gb.light3      = ['#bdb093', 248]     " 189-174-147
let s:gb.light4      = ['#a89d84', 246]     " 168-153-132
let s:gb.light4_256  = ['#a89984', 246]     " 168-153-132

let s:gb.bright_red     = ['#fb4934', 167]     " 251-73-52
let s:gb.bright_green   = ['#b8bb26', 142]     " 184-187-38
let s:gb.bright_yellow  = ['#fabd2f', 214]     " 250-189-47
let s:gb.bright_blue    = ['#83a598', 109]     " 131-165-152
let s:gb.bright_purple  = ['#d3869b', 175]     " 211-134-155
let s:gb.bright_aqua    = ['#8ec07c', 108]     " 142-192-124
let s:gb.bright_orange  = ['#fe8019', 208]     " 254-128-25

let s:gb.neutral_red    = ['#cc241d', 124]     " 204-36-29
let s:gb.neutral_green  = ['#98971a', 106]     " 152-151-26
let s:gb.neutral_yellow = ['#d79921', 172]     " 215-153-33
let s:gb.neutral_blue   = ['#458588', 66]      " 69-133-136
let s:gb.neutral_purple = ['#b16286', 132]     " 177-98-134
let s:gb.neutral_aqua   = ['#689d6a', 72]      " 104-157-106
let s:gb.neutral_orange = ['#d65d0e', 166]     " 214-93-14

let s:gb.faded_red      = ['#9d0006', 88]      " 157-0-6
let s:gb.faded_green    = ['#79740e', 100]     " 121-116-14
let s:gb.faded_yellow   = ['#b57614', 136]     " 181-118-20
let s:gb.faded_blue     = ['#076678', 24]      " 7-102-120
let s:gb.faded_purple   = ['#8f3f71', 96]      " 143-63-113
let s:gb.faded_aqua     = ['#427b58', 66]      " 66-123-88
let s:gb.faded_orange   = ['#af3a03', 130]     " 175-58-3

" }}}
" Setup Emphasis: {{{

let s:bold = 'bold,'
if g:gruvcrate_bold == 0
  let s:bold = ''
endif

let s:italic = 'italic,'
if g:gruvcrate_italic == 0
  let s:italic = ''
endif

let s:underline = 'underline,'
if g:gruvcrate_underline == 0
  let s:underline = ''
endif

let s:undercurl = 'undercurl,'
if g:gruvcrate_undercurl == 0
  let s:undercurl = ''
endif

let s:inverse = 'inverse,'
if g:gruvcrate_inverse == 0
  let s:inverse = ''
endif

" }}}
" Setup Colors: {{{

let s:vim_bg = ['bg', 'bg']
let s:vim_fg = ['fg', 'fg']
let s:none = ['NONE', 'NONE']

" determine relative colors
if s:is_dark
  let s:bg0  = s:gb.dark0
  if g:gruvcrate_contrast_dark == 'soft'
    let s:bg0  = s:gb.dark0_soft
  elseif g:gruvcrate_contrast_dark == 'hard'
    let s:bg0  = s:gb.dark0_hard
  endif

  let s:bg1  = s:gb.dark1
  let s:bg2  = s:gb.dark2
  let s:bg3  = s:gb.dark3
  let s:bg4  = s:gb.dark4

  let s:gray = s:gb.gray_245

  let s:fg0 = s:gb.light0
  let s:fg1 = s:gb.light1
  let s:fg2 = s:gb.light2
  let s:fg3 = s:gb.light3
  let s:fg4 = s:gb.light4

  let s:fg4_256 = s:gb.light4_256

  let s:red    = s:gb.bright_red
  let s:green  = s:gb.bright_green
  let s:yellow = s:gb.bright_yellow
  let s:blue   = s:gb.bright_blue
  let s:purple = s:gb.bright_purple
  let s:aqua   = s:gb.bright_aqua
  let s:orange = s:gb.bright_orange
else
  let s:bg0  = s:gb.light0
  if g:gruvcrate_contrast_light == 'soft'
    let s:bg0  = s:gb.light0_soft
  elseif g:gruvcrate_contrast_light == 'hard'
    let s:bg0  = s:gb.light0_hard
  endif

  let s:bg1  = s:gb.light1
  let s:bg2  = s:gb.light2
  let s:bg3  = s:gb.light3
  let s:bg4  = s:gb.light4

  let s:gray = s:gb.gray_244

  let s:fg0 = s:gb.dark0
  let s:fg1 = s:gb.dark1
  let s:fg2 = s:gb.dark2
  let s:fg3 = s:gb.dark3
  let s:fg4 = s:gb.dark4

  let s:fg4_256 = s:gb.dark4_256

  let s:red    = s:gb.faded_red
  let s:green  = s:gb.faded_green
  let s:yellow = s:gb.faded_yellow
  let s:blue   = s:gb.faded_blue
  let s:purple = s:gb.faded_purple
  let s:aqua   = s:gb.faded_aqua
  let s:orange = s:gb.faded_orange
endif

" reset to 16 colors fallback
if g:gruvcrate_termcolors == 16
  let s:bg0[1]    = 0
  let s:fg4[1]    = 7
  let s:gray[1]   = 8
  let s:red[1]    = 9
  let s:green[1]  = 10
  let s:yellow[1] = 11
  let s:blue[1]   = 12
  let s:purple[1] = 13
  let s:aqua[1]   = 14
  let s:fg1[1]    = 15
endif

" save current relative colors back to palette dictionary
let s:gb.bg0 = s:bg0
let s:gb.bg1 = s:bg1
let s:gb.bg2 = s:bg2
let s:gb.bg3 = s:bg3
let s:gb.bg4 = s:bg4

let s:gb.gray = s:gray

let s:gb.fg0 = s:fg0
let s:gb.fg1 = s:fg1
let s:gb.fg2 = s:fg2
let s:gb.fg3 = s:fg3
let s:gb.fg4 = s:fg4

let s:gb.fg4_256 = s:fg4_256

let s:gb.red    = s:red
let s:gb.green  = s:green
let s:gb.yellow = s:yellow
let s:gb.blue   = s:blue
let s:gb.purple = s:purple
let s:gb.aqua   = s:aqua
let s:gb.orange = s:orange

" }}}
" Setup Terminal Colors For Neovim: {{{

if has('nvim')
  let g:terminal_color_0 = s:bg0[0]
  let g:terminal_color_8 = s:gray[0]

  let g:terminal_color_1 = s:gb.neutral_red[0]
  let g:terminal_color_9 = s:red[0]

  let g:terminal_color_2 = s:gb.neutral_green[0]
  let g:terminal_color_10 = s:green[0]

  let g:terminal_color_3 = s:gb.neutral_yellow[0]
  let g:terminal_color_11 = s:yellow[0]

  let g:terminal_color_4 = s:gb.neutral_blue[0]
  let g:terminal_color_12 = s:blue[0]

  let g:terminal_color_5 = s:gb.neutral_purple[0]
  let g:terminal_color_13 = s:purple[0]

  let g:terminal_color_6 = s:gb.neutral_aqua[0]
  let g:terminal_color_14 = s:aqua[0]

  let g:terminal_color_7 = s:fg4[0]
  let g:terminal_color_15 = s:fg1[0]
endif

" }}}
" Overload Setting: {{{

let s:hls_cursor = s:orange
if exists('g:gruvcrate_hls_cursor')
  let s:hls_cursor = get(s:gb, g:gruvcrate_hls_cursor)
endif

let s:number_column = s:none
if exists('g:gruvcrate_number_column')
  let s:number_column = get(s:gb, g:gruvcrate_number_column)
endif

let s:sign_column = s:bg1

if exists('g:gitgutter_override_sign_column_highlight') &&
      \ g:gitgutter_override_sign_column_highlight == 1
  let s:sign_column = s:number_column
else
  let g:gitgutter_override_sign_column_highlight = 0

  if exists('g:gruvcrate_sign_column')
    let s:sign_column = get(s:gb, g:gruvcrate_sign_column)
  endif
endif

let s:color_column = s:bg1
if exists('g:gruvcrate_color_column')
  let s:color_column = get(s:gb, g:gruvcrate_color_column)
endif

let s:vert_split = s:bg0
if exists('g:gruvcrate_vert_split')
  let s:vert_split = get(s:gb, g:gruvcrate_vert_split)
endif

let s:invert_signs = ''
if exists('g:gruvcrate_invert_signs')
  if g:gruvcrate_invert_signs == 1
    let s:invert_signs = s:inverse
  endif
endif

let s:invert_selection = s:inverse
if exists('g:gruvcrate_invert_selection')
  if g:gruvcrate_invert_selection == 0
    let s:invert_selection = ''
  endif
endif

let s:invert_tabline = ''
if exists('g:gruvcrate_invert_tabline')
  if g:gruvcrate_invert_tabline == 1
    let s:invert_tabline = s:inverse
  endif
endif

let s:italicize_comments = s:italic
if exists('g:gruvcrate_italicize_comments')
  if g:gruvcrate_italicize_comments == 0
    let s:italicize_comments = ''
  endif
endif

let s:italicize_strings = ''
if exists('g:gruvcrate_italicize_strings')
  if g:gruvcrate_italicize_strings == 1
    let s:italicize_strings = s:italic
  endif
endif

" }}}
" Highlighting Function: {{{

function! s:HL(group, fg, ...)
  " Arguments: group, guifg, guibg, gui, guisp

  " foreground
  let fg = a:fg

  " background
  if a:0 >= 1
    let bg = a:1
  else
    let bg = s:none
  endif

  " emphasis
  if a:0 >= 2 && strlen(a:2)
    let emstr = a:2
  else
    let emstr = 'NONE,'
  endif

  " special fallback
  if a:0 >= 3
    if g:gruvcrate_guisp_fallback != 'NONE'
      let fg = a:3
    endif

    " bg fallback mode should invert higlighting
    if g:gruvcrate_guisp_fallback == 'bg'
      let emstr .= 'inverse,'
    endif
  endif

  let histring = [ 'hi', a:group,
        \ 'guifg=' . fg[0], 'ctermfg=' . fg[1],
        \ 'guibg=' . bg[0], 'ctermbg=' . bg[1],
        \ 'gui=' . emstr[:-2], 'cterm=' . emstr[:-2]
        \ ]

  " special
  if a:0 >= 3
    call add(histring, 'guisp=' . a:3[0])
  endif

  execute join(histring, ' ')
endfunction

" }}}
" Gruvcrate Hi Groups: {{{

" memoize common hi groups
call s:HL('GruvcrateFg0', s:fg0)
call s:HL('GruvcrateFg1', s:fg1)
call s:HL('GruvcrateFg2', s:fg2)
call s:HL('GruvcrateFg3', s:fg3)
call s:HL('GruvcrateFg4', s:fg4)
call s:HL('GruvcrateGray', s:gray)
call s:HL('GruvcrateBg0', s:bg0)
call s:HL('GruvcrateBg1', s:bg1)
call s:HL('GruvcrateBg2', s:bg2)
call s:HL('GruvcrateBg3', s:bg3)
call s:HL('GruvcrateBg4', s:bg4)

call s:HL('GruvcrateRed', s:red)
call s:HL('GruvcrateRedBold', s:red, s:none, s:bold)
call s:HL('GruvcrateGreen', s:green)
call s:HL('GruvcrateGreenBold', s:green, s:none, s:bold)
call s:HL('GruvcrateYellow', s:yellow)
call s:HL('GruvcrateYellowBold', s:yellow, s:none, s:bold)
call s:HL('GruvcrateBlue', s:blue)
call s:HL('GruvcrateBlueBold', s:blue, s:none, s:bold)
call s:HL('GruvcratePurple', s:purple)
call s:HL('GruvcratePurpleBold', s:purple, s:none, s:bold)
call s:HL('GruvcrateAqua', s:aqua)
call s:HL('GruvcrateAquaBold', s:aqua, s:none, s:bold)
call s:HL('GruvcrateOrange', s:orange)
call s:HL('GruvcrateOrangeBold', s:orange, s:none, s:bold)

call s:HL('GruvcrateRedSign', s:red, s:sign_column, s:invert_signs)
call s:HL('GruvcrateGreenSign', s:green, s:sign_column, s:invert_signs)
call s:HL('GruvcrateYellowSign', s:yellow, s:sign_column, s:invert_signs)
call s:HL('GruvcrateBlueSign', s:blue, s:sign_column, s:invert_signs)
call s:HL('GruvcratePurpleSign', s:purple, s:sign_column, s:invert_signs)
call s:HL('GruvcrateAquaSign', s:aqua, s:sign_column, s:invert_signs)
call s:HL('GruvcrateOrangeSign', s:orange, s:sign_column, s:invert_signs)

" }}}

" Vanilla colorscheme ---------------------------------------------------------
" General UI: {{{

" Normal text
call s:HL('Normal', s:fg1, s:bg0)

" Correct background (see issue #7):
" --- Problem with changing between dark and light on 256 color terminal
" --- https://github.com/morhetz/gruvcrate/issues/7
if s:is_dark
  set background=dark
else
  set background=light
endif

if version >= 700
  " Screen line that the cursor is
  call s:HL('CursorLine',   s:none, s:bg1)
  " Screen column that the cursor is
  hi! link CursorColumn CursorLine

  " Tab pages line filler
  call s:HL('TabLineFill', s:bg4, s:bg1, s:invert_tabline)
  " Active tab page label
  call s:HL('TabLineSel', s:green, s:bg1, s:invert_tabline)
  " Not active tab page label
  hi! link TabLine TabLineFill

  " Match paired bracket under the cursor
  call s:HL('MatchParen', s:none, s:bg3, s:bold)
endif

if version >= 703
  " Highlighted screen columns
  call s:HL('ColorColumn',  s:none, s:color_column)

  " Concealed element: \lambda → λ
  call s:HL('Conceal', s:blue, s:none)

  " Line number of CursorLine
  call s:HL('CursorLineNr', s:yellow, s:bg1)
endif

hi! link NonText GruvcrateBg2
hi! link SpecialKey GruvcrateBg2

call s:HL('Visual',    s:none,  s:bg3)
hi! link VisualNOS Visual

call s:HL('Search',    s:yellow, s:bg0, s:inverse)
call s:HL('IncSearch', s:hls_cursor, s:bg0, s:inverse)

call s:HL('Underlined', s:blue, s:none, s:underline)

call s:HL('StatusLine',   s:bg2, s:fg1, s:inverse)
call s:HL('StatusLineNC', s:bg1, s:fg4, s:inverse)

" The column separating vertically split windows
call s:HL('VertSplit', s:bg3, s:vert_split)

" Current match in wildmenu completion
call s:HL('WildMenu', s:blue, s:bg2, s:bold)

" Directory names, special names in listing
hi! link Directory GruvcrateGreenBold

" Titles for output from :set all, :autocmd, etc.
hi! link Title GruvcrateGreenBold

" Error messages on the command line
call s:HL('ErrorMsg',   s:bg0, s:red, s:bold)
" More prompt: -- More --
hi! link MoreMsg GruvcrateYellowBold
" Current mode message: -- INSERT --
hi! link ModeMsg GruvcrateYellowBold
" 'Press enter' prompt and yes/no questions
hi! link Question GruvcrateOrangeBold
" Warning messages
hi! link WarningMsg GruvcrateRedBold

" }}}
" Gutter: {{{

" Line number for :number and :# commands
call s:HL('LineNr', s:bg4, s:number_column)

" Column where signs are displayed
call s:HL('SignColumn', s:none, s:sign_column)

" Line used for closed folds
call s:HL('Folded', s:gray, s:bg1, s:italic)
" Column where folds are displayed
call s:HL('FoldColumn', s:gray, s:bg1)

" }}}
" Cursor: {{{

" Character under cursor
call s:HL('Cursor', s:none, s:none, s:inverse)
" Visual mode cursor, selection
hi! link vCursor Cursor
" Input moder cursor
hi! link iCursor Cursor
" Language mapping cursor
hi! link lCursor Cursor

" }}}
" Syntax Highlighting: {{{

if g:gruvcrate_improved_strings == 0
  hi! link Special GruvcrateOrange
else
  call s:HL('Special', s:orange, s:bg1, s:italicize_strings)
endif

call s:HL('Comment', s:gray, s:none, s:italicize_comments)
call s:HL('Todo', s:vim_fg, s:vim_bg, s:bold . s:italic)
call s:HL('Error', s:red, s:vim_bg, s:bold . s:inverse)

" Generic statement
hi! link Statement GruvcrateRed
" if, then, else, endif, swicth, etc.
hi! link Conditional GruvcrateRed
" for, do, while, etc.
hi! link Repeat GruvcrateRed
" case, default, etc.
hi! link Label GruvcrateRed
" try, catch, throw
hi! link Exception GruvcrateRed
" sizeof, "+", "*", etc.
hi! link Operator Normal
" Any other keyword
hi! link Keyword GruvcrateRed

" Variable name
hi! link Identifier GruvcrateBlue
" Function name
hi! link Function GruvcrateGreenBold

" Generic preprocessor
hi! link PreProc GruvcrateAqua
" Preprocessor #include
hi! link Include GruvcrateAqua
" Preprocessor #define
hi! link Define GruvcrateAqua
" Same as Define
hi! link Macro GruvcrateAqua
" Preprocessor #if, #else, #endif, etc.
hi! link PreCondit GruvcrateAqua

" Generic constant
hi! link Constant GruvcratePurple
" Character constant: 'c', '/n'
hi! link Character GruvcratePurple
" String constant: "this is a string"
if g:gruvcrate_improved_strings == 0
  call s:HL('String',  s:green, s:none, s:italicize_strings)
else
  call s:HL('String',  s:fg1, s:bg1, s:italicize_strings)
endif
" Boolean constant: TRUE, false
hi! link Boolean GruvcratePurple
" Number constant: 234, 0xff
hi! link Number GruvcratePurple
" Floating point constant: 2.3e10
hi! link Float GruvcratePurple

" Generic type
hi! link Type GruvcrateYellow
" static, register, volatile, etc
hi! link StorageClass GruvcrateOrange
" struct, union, enum, etc.
hi! link Structure GruvcrateAqua
" typedef
hi! link Typedef GruvcrateYellow

" }}}
" Completion Menu: {{{

if version >= 700
  " Popup menu: normal item
  call s:HL('Pmenu', s:fg1, s:bg2)
  " Popup menu: selected item
  call s:HL('PmenuSel', s:bg2, s:blue, s:bold)
  " Popup menu: scrollbar
  call s:HL('PmenuSbar', s:none, s:bg2)
  " Popup menu: scrollbar thumb
  call s:HL('PmenuThumb', s:none, s:bg4)
endif

" }}}
" Diffs: {{{

call s:HL('DiffDelete', s:red, s:bg0, s:inverse)
call s:HL('DiffAdd',    s:green, s:bg0, s:inverse)
"call s:HL('DiffChange', s:bg0, s:blue)
"call s:HL('DiffText',   s:bg0, s:yellow)

" Alternative setting
call s:HL('DiffChange', s:aqua, s:bg0, s:inverse)
call s:HL('DiffText',   s:yellow, s:bg0, s:inverse)

" }}}
" Spelling: {{{

if has("spell")
  " Not capitalised word, or compile warnings
  if g:gruvcrate_improved_warnings == 0
    call s:HL('SpellCap',   s:none, s:none, s:undercurl, s:red)
  else
    call s:HL('SpellCap',   s:green, s:none, s:bold . s:italic)
  endif
  " Not recognized word
  call s:HL('SpellBad',   s:none, s:none, s:undercurl, s:blue)
  " Wrong spelling for selected region
  call s:HL('SpellLocal', s:none, s:none, s:undercurl, s:aqua)
  " Rare word
  call s:HL('SpellRare',  s:none, s:none, s:undercurl, s:purple)
endif

" }}}

" Plugin specific -------------------------------------------------------------
" EasyMotion: {{{

hi! link EasyMotionTarget Search
hi! link EasyMotionShade Comment

" }}}
" Sneak: {{{

hi! link Sneak Search
hi! link SneakLabel Search

" }}}
" Indent Guides: {{{

if !exists('g:indent_guides_auto_colors')
  let g:indent_guides_auto_colors = 0
endif

if g:indent_guides_auto_colors == 0
  if g:gruvcrate_invert_indent_guides == 0
    call s:HL('IndentGuidesOdd', s:vim_bg, s:bg2)
    call s:HL('IndentGuidesEven', s:vim_bg, s:bg1)
  else
    call s:HL('IndentGuidesOdd', s:vim_bg, s:bg2, s:inverse)
    call s:HL('IndentGuidesEven', s:vim_bg, s:bg3, s:inverse)
  endif
endif

" }}}
" IndentLine: {{{

if !exists('g:indentLine_color_term')
  let g:indentLine_color_term = s:bg2[1]
endif
if !exists('g:indentLine_color_gui')
  let g:indentLine_color_gui = s:bg2[0]
endif

" }}}
" Rainbow Parentheses: {{{

if !exists('g:rbpt_colorpairs')
  let g:rbpt_colorpairs =
    \ [
      \ ['blue', '#458588'], ['magenta', '#b16286'],
      \ ['red',  '#cc241d'], ['166',     '#d65d0e']
    \ ]
endif

let g:rainbow_guifgs = [ '#d65d0e', '#cc241d', '#b16286', '#458588' ]
let g:rainbow_ctermfgs = [ '166', 'red', 'magenta', 'blue' ]

if !exists('g:rainbow_conf')
   let g:rainbow_conf = {}
endif
if !has_key(g:rainbow_conf, 'guifgs')
   let g:rainbow_conf['guifgs'] = g:rainbow_guifgs
endif
if !has_key(g:rainbow_conf, 'ctermfgs')
   let g:rainbow_conf['ctermfgs'] = g:rainbow_ctermfgs
endif

let g:niji_dark_colours = g:rbpt_colorpairs
let g:niji_light_colours = g:rbpt_colorpairs

"}}}
" GitGutter: {{{

hi! link GitGutterAdd GruvcrateGreenSign
hi! link GitGutterChange GruvcrateAquaSign
hi! link GitGutterDelete GruvcrateRedSign
hi! link GitGutterChangeDelete GruvcrateAquaSign

" }}}
" GitCommit: "{{{

hi! link gitcommitSelectedFile GruvcrateGreen
hi! link gitcommitDiscardedFile GruvcrateRed

" }}}
" Signify: {{{

hi! link SignifySignAdd GruvcrateGreenSign
hi! link SignifySignChange GruvcrateAquaSign
hi! link SignifySignDelete GruvcrateRedSign

" }}}
" Syntastic: {{{

call s:HL('SyntasticError', s:none, s:none, s:undercurl, s:red)
call s:HL('SyntasticWarning', s:none, s:none, s:undercurl, s:yellow)

hi! link SyntasticErrorSign GruvcrateRedSign
hi! link SyntasticWarningSign GruvcrateYellowSign

" }}}
" Signature: {{{
hi! link SignatureMarkText   GruvcrateBlueSign
hi! link SignatureMarkerText GruvcratePurpleSign

" }}}
" ShowMarks: {{{

hi! link ShowMarksHLl GruvcrateBlueSign
hi! link ShowMarksHLu GruvcrateBlueSign
hi! link ShowMarksHLo GruvcrateBlueSign
hi! link ShowMarksHLm GruvcrateBlueSign

" }}}
" CtrlP: {{{

hi! link CtrlPMatch GruvcrateYellow
hi! link CtrlPNoEntries GruvcrateRed
hi! link CtrlPPrtBase GruvcrateBg2
hi! link CtrlPPrtCursor GruvcrateBlue
hi! link CtrlPLinePre GruvcrateBg2

call s:HL('CtrlPMode1', s:blue, s:bg2, s:bold)
call s:HL('CtrlPMode2', s:bg0, s:blue, s:bold)
call s:HL('CtrlPStats', s:fg4, s:bg2, s:bold)

" }}}
" Startify: {{{

hi! link StartifyBracket GruvcrateFg3
hi! link StartifyFile GruvcrateFg1
hi! link StartifyNumber GruvcrateBlue
hi! link StartifyPath GruvcrateGray
hi! link StartifySlash GruvcrateGray
hi! link StartifySection GruvcrateYellow
hi! link StartifySpecial GruvcrateBg2
hi! link StartifyHeader GruvcrateOrange
hi! link StartifyFooter GruvcrateBg2

" }}}
" Vimshell: {{{

let g:vimshell_escape_colors = [
  \ s:bg4[0], s:red[0], s:green[0], s:yellow[0],
  \ s:blue[0], s:purple[0], s:aqua[0], s:fg4[0],
  \ s:bg0[0], s:red[0], s:green[0], s:orange[0],
  \ s:blue[0], s:purple[0], s:aqua[0], s:fg0[0]
  \ ]

" }}}
" BufTabLine: {{{

call s:HL('BufTabLineCurrent', s:bg0, s:fg4)
call s:HL('BufTabLineActive', s:fg4, s:bg2)
call s:HL('BufTabLineHidden', s:bg4, s:bg1)
call s:HL('BufTabLineFill', s:bg0, s:bg0)

" }}}
" Asynchronous Lint Engine: {{{

call s:HL('ALEError', s:none, s:none, s:undercurl, s:red)
call s:HL('ALEWarning', s:none, s:none, s:undercurl, s:yellow)
call s:HL('ALEInfo', s:none, s:none, s:undercurl, s:blue)

hi! link ALEErrorSign GruvcrateRedSign
hi! link ALEWarningSign GruvcrateYellowSign
hi! link ALEInfoSign GruvcrateBlueSign

" }}}
" Dirvish: {{{

hi! link DirvishPathTail GruvcrateAqua
hi! link DirvishArg GruvcrateYellow

" }}}
" Netrw: {{{

hi! link netrwDir GruvcrateAqua
hi! link netrwClassify GruvcrateAqua
hi! link netrwLink GruvcrateGray
hi! link netrwSymLink GruvcrateFg1
hi! link netrwExe GruvcrateYellow
hi! link netrwComment GruvcrateGray
hi! link netrwList GruvcrateBlue
hi! link netrwHelpCmd GruvcrateAqua
hi! link netrwCmdSep GruvcrateFg3
hi! link netrwVersion GruvcrateGreen

" }}}
" NERDTree: {{{

hi! link NERDTreeDir GruvcrateAqua
hi! link NERDTreeDirSlash GruvcrateAqua

hi! link NERDTreeOpenable GruvcrateOrange
hi! link NERDTreeClosable GruvcrateOrange

hi! link NERDTreeFile GruvcrateFg1
hi! link NERDTreeExecFile GruvcrateYellow

hi! link NERDTreeUp GruvcrateGray
hi! link NERDTreeCWD GruvcrateGreen
hi! link NERDTreeHelp GruvcrateFg1

hi! link NERDTreeToggleOn GruvcrateGreen
hi! link NERDTreeToggleOff GruvcrateRed

" }}}
" Vim Multiple Cursors: {{{

call s:HL('multiple_cursors_cursor', s:none, s:none, s:inverse)
call s:HL('multiple_cursors_visual', s:none, s:bg2)

" }}}
" coc.nvim: {{{

hi! link CocErrorSign GruvcrateRedSign
hi! link CocWarningSign GruvcrateOrangeSign
hi! link CocInfoSign GruvcrateYellowSign
hi! link CocHintSign GruvcrateBlueSign
hi! link CocErrorFloat GruvcrateRed
hi! link CocWarningFloat GruvcrateOrange
hi! link CocInfoFloat GruvcrateYellow
hi! link CocHintFloat GruvcrateBlue
hi! link CocDiagnosticsError GruvcrateRed
hi! link CocDiagnosticsWarning GruvcrateOrange
hi! link CocDiagnosticsInfo GruvcrateYellow
hi! link CocDiagnosticsHint GruvcrateBlue

hi! link CocSelectedText GruvcrateRed
hi! link CocCodeLens GruvcrateGray

call s:HL('CocErrorHighlight', s:none, s:none, s:undercurl, s:red)
call s:HL('CocWarningHighlight', s:none, s:none, s:undercurl, s:orange)
call s:HL('CocInfoHighlight', s:none, s:none, s:undercurl, s:yellow)
call s:HL('CocHintHighlight', s:none, s:none, s:undercurl, s:blue)

" }}}

" Filetype specific -----------------------------------------------------------
" Diff: {{{

hi! link diffAdded GruvcrateGreen
hi! link diffRemoved GruvcrateRed
hi! link diffChanged GruvcrateAqua

hi! link diffFile GruvcrateOrange
hi! link diffNewFile GruvcrateYellow

hi! link diffLine GruvcrateBlue

" }}}
" Html: {{{

hi! link htmlTag GruvcrateBlue
hi! link htmlEndTag GruvcrateBlue

hi! link htmlTagName GruvcrateAquaBold
hi! link htmlArg GruvcrateAqua

hi! link htmlScriptTag GruvcratePurple
hi! link htmlTagN GruvcrateFg1
hi! link htmlSpecialTagName GruvcrateAquaBold

call s:HL('htmlLink', s:fg4, s:none, s:underline)

hi! link htmlSpecialChar GruvcrateOrange

call s:HL('htmlBold', s:vim_fg, s:vim_bg, s:bold)
call s:HL('htmlBoldUnderline', s:vim_fg, s:vim_bg, s:bold . s:underline)
call s:HL('htmlBoldItalic', s:vim_fg, s:vim_bg, s:bold . s:italic)
call s:HL('htmlBoldUnderlineItalic', s:vim_fg, s:vim_bg, s:bold . s:underline . s:italic)

call s:HL('htmlUnderline', s:vim_fg, s:vim_bg, s:underline)
call s:HL('htmlUnderlineItalic', s:vim_fg, s:vim_bg, s:underline . s:italic)
call s:HL('htmlItalic', s:vim_fg, s:vim_bg, s:italic)

" }}}
" Xml: {{{

hi! link xmlTag GruvcrateBlue
hi! link xmlEndTag GruvcrateBlue
hi! link xmlTagName GruvcrateBlue
hi! link xmlEqual GruvcrateBlue
hi! link docbkKeyword GruvcrateAquaBold

hi! link xmlDocTypeDecl GruvcrateGray
hi! link xmlDocTypeKeyword GruvcratePurple
hi! link xmlCdataStart GruvcrateGray
hi! link xmlCdataCdata GruvcratePurple
hi! link dtdFunction GruvcrateGray
hi! link dtdTagName GruvcratePurple

hi! link xmlAttrib GruvcrateAqua
hi! link xmlProcessingDelim GruvcrateGray
hi! link dtdParamEntityPunct GruvcrateGray
hi! link dtdParamEntityDPunct GruvcrateGray
hi! link xmlAttribPunct GruvcrateGray

hi! link xmlEntity GruvcrateOrange
hi! link xmlEntityPunct GruvcrateOrange
" }}}
" Vim: {{{

call s:HL('vimCommentTitle', s:fg4_256, s:none, s:bold . s:italicize_comments)

hi! link vimNotation GruvcrateOrange
hi! link vimBracket GruvcrateOrange
hi! link vimMapModKey GruvcrateOrange
hi! link vimFuncSID GruvcrateFg3
hi! link vimSetSep GruvcrateFg3
hi! link vimSep GruvcrateFg3
hi! link vimContinue GruvcrateFg3

" }}}
" Clojure: {{{

hi! link clojureKeyword GruvcrateBlue
hi! link clojureCond GruvcrateOrange
hi! link clojureSpecial GruvcrateOrange
hi! link clojureDefine GruvcrateOrange

hi! link clojureFunc GruvcrateYellow
hi! link clojureRepeat GruvcrateYellow
hi! link clojureCharacter GruvcrateAqua
hi! link clojureStringEscape GruvcrateAqua
hi! link clojureException GruvcrateRed

hi! link clojureRegexp GruvcrateAqua
hi! link clojureRegexpEscape GruvcrateAqua
call s:HL('clojureRegexpCharClass', s:fg3, s:none, s:bold)
hi! link clojureRegexpMod clojureRegexpCharClass
hi! link clojureRegexpQuantifier clojureRegexpCharClass

hi! link clojureParen GruvcrateFg3
hi! link clojureAnonArg GruvcrateYellow
hi! link clojureVariable GruvcrateBlue
hi! link clojureMacro GruvcrateOrange

hi! link clojureMeta GruvcrateYellow
hi! link clojureDeref GruvcrateYellow
hi! link clojureQuote GruvcrateYellow
hi! link clojureUnquote GruvcrateYellow

" }}}
" C: {{{

hi! link cOperator GruvcratePurple
hi! link cStructure GruvcrateOrange

" }}}
" Python: {{{

hi! link pythonBuiltin GruvcrateOrange
hi! link pythonBuiltinObj GruvcrateOrange
hi! link pythonBuiltinFunc GruvcrateOrange
hi! link pythonFunction GruvcrateAqua
hi! link pythonDecorator GruvcrateRed
hi! link pythonInclude GruvcrateBlue
hi! link pythonImport GruvcrateBlue
hi! link pythonRun GruvcrateBlue
hi! link pythonCoding GruvcrateBlue
hi! link pythonOperator GruvcrateRed
hi! link pythonException GruvcrateRed
hi! link pythonExceptions GruvcratePurple
hi! link pythonBoolean GruvcratePurple
hi! link pythonDot GruvcrateFg3
hi! link pythonConditional GruvcrateRed
hi! link pythonRepeat GruvcrateRed
hi! link pythonDottedName GruvcrateGreenBold

" }}}
" CSS: {{{

hi! link cssBraces GruvcrateBlue
hi! link cssFunctionName GruvcrateYellow
hi! link cssIdentifier GruvcrateOrange
hi! link cssClassName GruvcrateGreen
hi! link cssColor GruvcrateBlue
hi! link cssSelectorOp GruvcrateBlue
hi! link cssSelectorOp2 GruvcrateBlue
hi! link cssImportant GruvcrateGreen
hi! link cssVendor GruvcrateFg1

hi! link cssTextProp GruvcrateAqua
hi! link cssAnimationProp GruvcrateAqua
hi! link cssUIProp GruvcrateYellow
hi! link cssTransformProp GruvcrateAqua
hi! link cssTransitionProp GruvcrateAqua
hi! link cssPrintProp GruvcrateAqua
hi! link cssPositioningProp GruvcrateYellow
hi! link cssBoxProp GruvcrateAqua
hi! link cssFontDescriptorProp GruvcrateAqua
hi! link cssFlexibleBoxProp GruvcrateAqua
hi! link cssBorderOutlineProp GruvcrateAqua
hi! link cssBackgroundProp GruvcrateAqua
hi! link cssMarginProp GruvcrateAqua
hi! link cssListProp GruvcrateAqua
hi! link cssTableProp GruvcrateAqua
hi! link cssFontProp GruvcrateAqua
hi! link cssPaddingProp GruvcrateAqua
hi! link cssDimensionProp GruvcrateAqua
hi! link cssRenderProp GruvcrateAqua
hi! link cssColorProp GruvcrateAqua
hi! link cssGeneratedContentProp GruvcrateAqua

" }}}
" JavaScript: {{{

hi! link javaScriptBraces GruvcrateFg1
hi! link javaScriptFunction GruvcrateAqua
hi! link javaScriptIdentifier GruvcrateRed
hi! link javaScriptMember GruvcrateBlue
hi! link javaScriptNumber GruvcratePurple
hi! link javaScriptNull GruvcratePurple
hi! link javaScriptParens GruvcrateFg3

" }}}
" YAJS: {{{

hi! link javascriptImport GruvcrateAqua
hi! link javascriptExport GruvcrateAqua
hi! link javascriptClassKeyword GruvcrateAqua
hi! link javascriptClassExtends GruvcrateAqua
hi! link javascriptDefault GruvcrateAqua

hi! link javascriptClassName GruvcrateYellow
hi! link javascriptClassSuperName GruvcrateYellow
hi! link javascriptGlobal GruvcrateYellow

hi! link javascriptEndColons GruvcrateFg1
hi! link javascriptFuncArg GruvcrateFg1
hi! link javascriptGlobalMethod GruvcrateFg1
hi! link javascriptNodeGlobal GruvcrateFg1
hi! link javascriptBOMWindowProp GruvcrateFg1
hi! link javascriptArrayMethod GruvcrateFg1
hi! link javascriptArrayStaticMethod GruvcrateFg1
hi! link javascriptCacheMethod GruvcrateFg1
hi! link javascriptDateMethod GruvcrateFg1
hi! link javascriptMathStaticMethod GruvcrateFg1

" hi! link javascriptProp GruvcrateFg1
hi! link javascriptURLUtilsProp GruvcrateFg1
hi! link javascriptBOMNavigatorProp GruvcrateFg1
hi! link javascriptDOMDocMethod GruvcrateFg1
hi! link javascriptDOMDocProp GruvcrateFg1
hi! link javascriptBOMLocationMethod GruvcrateFg1
hi! link javascriptBOMWindowMethod GruvcrateFg1
hi! link javascriptStringMethod GruvcrateFg1

hi! link javascriptVariable GruvcrateOrange
" hi! link javascriptVariable GruvcrateRed
" hi! link javascriptIdentifier GruvcrateOrange
" hi! link javascriptClassSuper GruvcrateOrange
hi! link javascriptIdentifier GruvcrateOrange
hi! link javascriptClassSuper GruvcrateOrange

" hi! link javascriptFuncKeyword GruvcrateOrange
" hi! link javascriptAsyncFunc GruvcrateOrange
hi! link javascriptFuncKeyword GruvcrateAqua
hi! link javascriptAsyncFunc GruvcrateAqua
hi! link javascriptClassStatic GruvcrateOrange

hi! link javascriptOperator GruvcrateRed
hi! link javascriptForOperator GruvcrateRed
hi! link javascriptYield GruvcrateRed
hi! link javascriptExceptions GruvcrateRed
hi! link javascriptMessage GruvcrateRed

hi! link javascriptTemplateSB GruvcrateAqua
hi! link javascriptTemplateSubstitution GruvcrateFg1

" hi! link javascriptLabel GruvcrateBlue
" hi! link javascriptObjectLabel GruvcrateBlue
" hi! link javascriptPropertyName GruvcrateBlue
hi! link javascriptLabel GruvcrateFg1
hi! link javascriptObjectLabel GruvcrateFg1
hi! link javascriptPropertyName GruvcrateFg1

hi! link javascriptLogicSymbols GruvcrateFg1
hi! link javascriptArrowFunc GruvcrateYellow

hi! link javascriptDocParamName GruvcrateFg4
hi! link javascriptDocTags GruvcrateFg4
hi! link javascriptDocNotation GruvcrateFg4
hi! link javascriptDocParamType GruvcrateFg4
hi! link javascriptDocNamedParamType GruvcrateFg4

hi! link javascriptBrackets GruvcrateFg1
hi! link javascriptDOMElemAttrs GruvcrateFg1
hi! link javascriptDOMEventMethod GruvcrateFg1
hi! link javascriptDOMNodeMethod GruvcrateFg1
hi! link javascriptDOMStorageMethod GruvcrateFg1
hi! link javascriptHeadersMethod GruvcrateFg1

hi! link javascriptAsyncFuncKeyword GruvcrateRed
hi! link javascriptAwaitFuncKeyword GruvcrateRed

" }}}
" PanglossJS: {{{

hi! link jsClassKeyword GruvcrateAqua
hi! link jsExtendsKeyword GruvcrateAqua
hi! link jsExportDefault GruvcrateAqua
hi! link jsTemplateBraces GruvcrateAqua
hi! link jsGlobalNodeObjects GruvcrateFg1
hi! link jsGlobalObjects GruvcrateFg1
hi! link jsFunction GruvcrateAqua
hi! link jsFuncParens GruvcrateFg3
hi! link jsParens GruvcrateFg3
hi! link jsNull GruvcratePurple
hi! link jsUndefined GruvcratePurple
hi! link jsClassDefinition GruvcrateYellow

" }}}
" TypeScript: {{{

hi! link typeScriptReserved GruvcrateAqua
hi! link typeScriptLabel GruvcrateAqua
hi! link typeScriptFuncKeyword GruvcrateAqua
hi! link typeScriptIdentifier GruvcrateOrange
hi! link typeScriptBraces GruvcrateFg1
hi! link typeScriptEndColons GruvcrateFg1
hi! link typeScriptDOMObjects GruvcrateFg1
hi! link typeScriptAjaxMethods GruvcrateFg1
hi! link typeScriptLogicSymbols GruvcrateFg1
hi! link typeScriptDocSeeTag Comment
hi! link typeScriptDocParam Comment
hi! link typeScriptDocTags vimCommentTitle
hi! link typeScriptGlobalObjects GruvcrateFg1
hi! link typeScriptParens GruvcrateFg3
hi! link typeScriptOpSymbols GruvcrateFg3
hi! link typeScriptHtmlElemProperties GruvcrateFg1
hi! link typeScriptNull GruvcratePurple
hi! link typeScriptInterpolationDelimiter GruvcrateAqua

" }}}
" PureScript: {{{

hi! link purescriptModuleKeyword GruvcrateAqua
hi! link purescriptModuleName GruvcrateFg1
hi! link purescriptWhere GruvcrateAqua
hi! link purescriptDelimiter GruvcrateFg4
hi! link purescriptType GruvcrateFg1
hi! link purescriptImportKeyword GruvcrateAqua
hi! link purescriptHidingKeyword GruvcrateAqua
hi! link purescriptAsKeyword GruvcrateAqua
hi! link purescriptStructure GruvcrateAqua
hi! link purescriptOperator GruvcrateBlue

hi! link purescriptTypeVar GruvcrateFg1
hi! link purescriptConstructor GruvcrateFg1
hi! link purescriptFunction GruvcrateFg1
hi! link purescriptConditional GruvcrateOrange
hi! link purescriptBacktick GruvcrateOrange

" }}}
" CoffeeScript: {{{

hi! link coffeeExtendedOp GruvcrateFg3
hi! link coffeeSpecialOp GruvcrateFg3
hi! link coffeeCurly GruvcrateOrange
hi! link coffeeParen GruvcrateFg3
hi! link coffeeBracket GruvcrateOrange

" }}}
" Ruby: {{{

hi! link rubyStringDelimiter GruvcrateGreen
hi! link rubyInterpolationDelimiter GruvcrateAqua

" }}}
" ObjectiveC: {{{

hi! link objcTypeModifier GruvcrateRed
hi! link objcDirective GruvcrateBlue

" }}}
" Go: {{{

hi! link goDirective GruvcrateAqua
hi! link goConstants GruvcratePurple
hi! link goDeclaration GruvcrateRed
hi! link goDeclType GruvcrateBlue
hi! link goBuiltins GruvcrateOrange

" }}}
" Lua: {{{

hi! link luaIn GruvcrateRed
hi! link luaFunction GruvcrateAqua
hi! link luaTable GruvcrateOrange

" }}}
" MoonScript: {{{

hi! link moonSpecialOp GruvcrateFg3
hi! link moonExtendedOp GruvcrateFg3
hi! link moonFunction GruvcrateFg3
hi! link moonObject GruvcrateYellow

" }}}
" Java: {{{

hi! link javaAnnotation GruvcrateBlue
hi! link javaDocTags GruvcrateAqua
hi! link javaCommentTitle vimCommentTitle
hi! link javaParen GruvcrateFg3
hi! link javaParen1 GruvcrateFg3
hi! link javaParen2 GruvcrateFg3
hi! link javaParen3 GruvcrateFg3
hi! link javaParen4 GruvcrateFg3
hi! link javaParen5 GruvcrateFg3
hi! link javaOperator GruvcrateOrange

hi! link javaVarArg GruvcrateGreen

" }}}
" Elixir: {{{

hi! link elixirDocString Comment

hi! link elixirStringDelimiter GruvcrateGreen
hi! link elixirInterpolationDelimiter GruvcrateAqua

hi! link elixirModuleDeclaration GruvcrateYellow

" }}}
" Scala: {{{

" NB: scala vim syntax file is kinda horrible
hi! link scalaNameDefinition GruvcrateFg1
hi! link scalaCaseFollowing GruvcrateFg1
hi! link scalaCapitalWord GruvcrateFg1
hi! link scalaTypeExtension GruvcrateFg1

hi! link scalaKeyword GruvcrateRed
hi! link scalaKeywordModifier GruvcrateRed

hi! link scalaSpecial GruvcrateAqua
hi! link scalaOperator GruvcrateFg1

hi! link scalaTypeDeclaration GruvcrateYellow
hi! link scalaTypeTypePostDeclaration GruvcrateYellow

hi! link scalaInstanceDeclaration GruvcrateFg1
hi! link scalaInterpolation GruvcrateAqua

" }}}
" Markdown: {{{

call s:HL('markdownItalic', s:fg3, s:none, s:italic)

hi! link markdownH1 GruvcrateGreenBold
hi! link markdownH2 GruvcrateGreenBold
hi! link markdownH3 GruvcrateYellowBold
hi! link markdownH4 GruvcrateYellowBold
hi! link markdownH5 GruvcrateYellow
hi! link markdownH6 GruvcrateYellow

hi! link markdownCode GruvcrateAqua
hi! link markdownCodeBlock GruvcrateAqua
hi! link markdownCodeDelimiter GruvcrateAqua

hi! link markdownBlockquote GruvcrateGray
hi! link markdownListMarker GruvcrateGray
hi! link markdownOrderedListMarker GruvcrateGray
hi! link markdownRule GruvcrateGray
hi! link markdownHeadingRule GruvcrateGray

hi! link markdownUrlDelimiter GruvcrateFg3
hi! link markdownLinkDelimiter GruvcrateFg3
hi! link markdownLinkTextDelimiter GruvcrateFg3

hi! link markdownHeadingDelimiter GruvcrateOrange
hi! link markdownUrl GruvcratePurple
hi! link markdownUrlTitleDelimiter GruvcrateGreen

call s:HL('markdownLinkText', s:gray, s:none, s:underline)
hi! link markdownIdDeclaration markdownLinkText

" }}}
" Haskell: {{{

" hi! link haskellType GruvcrateYellow
" hi! link haskellOperators GruvcrateOrange
" hi! link haskellConditional GruvcrateAqua
" hi! link haskellLet GruvcrateOrange
"
hi! link haskellType GruvcrateFg1
hi! link haskellIdentifier GruvcrateFg1
hi! link haskellSeparator GruvcrateFg1
hi! link haskellDelimiter GruvcrateFg4
hi! link haskellOperators GruvcrateBlue
"
hi! link haskellBacktick GruvcrateOrange
hi! link haskellStatement GruvcrateOrange
hi! link haskellConditional GruvcrateOrange

hi! link haskellLet GruvcrateAqua
hi! link haskellDefault GruvcrateAqua
hi! link haskellWhere GruvcrateAqua
hi! link haskellBottom GruvcrateAqua
hi! link haskellBlockKeywords GruvcrateAqua
hi! link haskellImportKeywords GruvcrateAqua
hi! link haskellDeclKeyword GruvcrateAqua
hi! link haskellDeriving GruvcrateAqua
hi! link haskellAssocType GruvcrateAqua

hi! link haskellNumber GruvcratePurple
hi! link haskellPragma GruvcratePurple

hi! link haskellString GruvcrateGreen
hi! link haskellChar GruvcrateGreen

" }}}
" Json: {{{

hi! link jsonKeyword GruvcrateGreen
hi! link jsonQuote GruvcrateGreen
hi! link jsonBraces GruvcrateFg1
hi! link jsonString GruvcrateFg1

" }}}


" Functions -------------------------------------------------------------------
" Search Highlighting Cursor {{{

function! GruvcrateHlsShowCursor()
  call s:HL('Cursor', s:bg0, s:hls_cursor)
endfunction

function! GruvcrateHlsHideCursor()
  call s:HL('Cursor', s:none, s:none, s:inverse)
endfunction

" }}}

" vim: set sw=2 ts=2 sts=2 et tw=80 ft=vim fdm=marker:
